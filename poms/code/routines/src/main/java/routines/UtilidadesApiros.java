package routines;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


import org.apache.commons.codec.binary.Base64;






/*
 * user specification: the function's comment should contain keys as follows: 1. write about the function's comment.but
 * it must be before the "{talendTypes}" key.
 * 
 * 2. {talendTypes} 's value must be talend Type, it is required . its value should be one of: String, char | Character,
 * long | Long, int | Integer, boolean | Boolean, byte | Byte, Date, double | Double, float | Float, Object, short |
 * Short
 * 
 * 3. {Category} define a category for the Function. it is required. its value is user-defined .
 * 
 * 4. {param} 's format is: {param} <type>[(<default value or closed list values>)] <name>[ : <comment>]
 * 
 * <type> 's value should be one of: string, int, list, double, object, boolean, long, char, date. <name>'s value is the
 * Function's parameter name. the {param} is optional. so if you the Function without the parameters. the {param} don't
 * added. you can have many parameters for the Function.
 * 
 * 5. {example} gives a example for the Function. it is optional.
 */
public class UtilidadesApiros {
	private static byte[] archivo64;

    /**
     * helloExample: not return value, only print "hello" + message.
     * 
     * 
     * {talendTypes} String
     * 
     * {Category} User Defined
     * 
     * {param} string("world") input: The string need to be printed.
     * 
     * {example} helloExemple("world") # hello world !.
     */
    public static void helloExample(String message) {
        if (message == null) {
            message = "World"; //$NON-NLS-1$
        }
        System.out.println("Hello " + message + " !"); //$NON-NLS-1$ //$NON-NLS-2$
    }
    /**
     * helloExample: not return value, only print "hello" + message.
     * 
     * 
     * {talendTypes} String
     * 
     * {Category} User Defined
     * 
     * {param} string("world") input: The string need to be printed.
     * 
     * {example} helloExemple("world") # hello world !.
     */
    public static String cambioFecha (String fecha) {
    	String format = "yyyy-MM-dd";
    	String strDate=null;
    	try {
			Date fecha2 = new SimpleDateFormat(format).parse(fecha);
			DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");  
			strDate = dateFormat.format(fecha2);  
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return strDate;
		}
    /**
     * helloExample: not return value, only print "hello" + message.
     * 
     * 
     * {talendTypes} String
     * 
     * {Category} User Defined
     * 
     * {param} string("world") input: The string need to be printed.
     * 
     * {example} helloExemple("world") # hello world !.
     */
    public static String separaNumeral (String ListaCompradores){
    	String[] ListaSepara = ListaCompradores.split("#");
    	String ListaSeparados = "";    	
    	for(String item : ListaSepara)
    	{
    	  ListaSeparados= ListaSeparados+item;
    	}    	
    	
		return ListaSeparados;    	    	
    }
    /**
     * separaNumeroAutoriz:  return value of numeration from a String of numeroautorizacion.
     *      * 
     * {talendTypes} String
     * 
     * {Category} User Defined
     * 
     * {param} string("world") input: The string need to be printed.
     * 
     * {example} helloExemple("world") # hello world !.
     */
    public static String separaNumeroAutoriz (String NumeroAutorizacion){
		String[] TextoDian = NumeroAutorizacion.split(" ");		  	
    	return TextoDian[2];    	
    }
    /**
     * lecturaBase64:  return value of numeration from a String of numeroautorizacion.
     *      * 
     * {talendTypes} String
     * 
     * {Category} User Defined
     * 
     * {param} string("world") input: The string need to be printed.
     * 
     * {example} helloExemple("world") # hello world !.
     */  
    public static byte[] lecturaBase64(String path) throws IOException {
		
		File dataFile = new File(path);
		//System.out.println(dataFile.exists() + " is deleted!_2");
		
		
		if(dataFile.exists()){
			FileInputStream fis = new FileInputStream(dataFile);
			byte byteArray[] = new byte[(int)dataFile.length()];
			fis.read(byteArray);
			archivo64= Base64.encodeBase64(byteArray);	
			
			//byte[] archivo64 = FileUtils.readFileToByteArray(dataFile);
			
			fis.close();
			
			//System.out.println(dataFile.exists() + " is deleted!_3");
		}
		
		return archivo64;
	
	}
    
}
